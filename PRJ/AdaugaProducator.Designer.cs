﻿namespace LicentaFINAL
{
    partial class AdaugaProducator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNume = new System.Windows.Forms.Label();
            this.lbTara = new System.Windows.Forms.Label();
            this.lbCodFiscal = new System.Windows.Forms.Label();
            this.lbMail = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbNume = new System.Windows.Forms.TextBox();
            this.tbTara = new System.Windows.Forms.TextBox();
            this.tbCodFiscal = new System.Windows.Forms.TextBox();
            this.tbMail = new System.Windows.Forms.TextBox();
            this.btnAddProducator = new System.Windows.Forms.Button();
            this.lbClose = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbNume
            // 
            this.lbNume.AutoSize = true;
            this.lbNume.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNume.Location = new System.Drawing.Point(44, 95);
            this.lbNume.Name = "lbNume";
            this.lbNume.Size = new System.Drawing.Size(63, 20);
            this.lbNume.TabIndex = 0;
            this.lbNume.Text = "Nume: ";
            // 
            // lbTara
            // 
            this.lbTara.AutoSize = true;
            this.lbTara.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTara.Location = new System.Drawing.Point(44, 140);
            this.lbTara.Name = "lbTara";
            this.lbTara.Size = new System.Drawing.Size(53, 20);
            this.lbTara.TabIndex = 1;
            this.lbTara.Text = "Tara: ";
            // 
            // lbCodFiscal
            // 
            this.lbCodFiscal.AutoSize = true;
            this.lbCodFiscal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCodFiscal.Location = new System.Drawing.Point(44, 181);
            this.lbCodFiscal.Name = "lbCodFiscal";
            this.lbCodFiscal.Size = new System.Drawing.Size(99, 20);
            this.lbCodFiscal.TabIndex = 2;
            this.lbCodFiscal.Text = "Cod Fiscal: ";
            // 
            // lbMail
            // 
            this.lbMail.AutoSize = true;
            this.lbMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMail.Location = new System.Drawing.Point(44, 226);
            this.lbMail.Name = "lbMail";
            this.lbMail.Size = new System.Drawing.Size(60, 20);
            this.lbMail.TabIndex = 3;
            this.lbMail.Text = "e-mail:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(28, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(251, 49);
            this.label5.TabIndex = 4;
            this.label5.Text = "Producator Nou";
            // 
            // tbNume
            // 
            this.tbNume.Location = new System.Drawing.Point(154, 93);
            this.tbNume.Name = "tbNume";
            this.tbNume.Size = new System.Drawing.Size(100, 22);
            this.tbNume.TabIndex = 5;
            // 
            // tbTara
            // 
            this.tbTara.Location = new System.Drawing.Point(154, 138);
            this.tbTara.Name = "tbTara";
            this.tbTara.Size = new System.Drawing.Size(100, 22);
            this.tbTara.TabIndex = 6;
            // 
            // tbCodFiscal
            // 
            this.tbCodFiscal.Location = new System.Drawing.Point(154, 179);
            this.tbCodFiscal.Name = "tbCodFiscal";
            this.tbCodFiscal.Size = new System.Drawing.Size(100, 22);
            this.tbCodFiscal.TabIndex = 7;
            // 
            // tbMail
            // 
            this.tbMail.Location = new System.Drawing.Point(154, 224);
            this.tbMail.Name = "tbMail";
            this.tbMail.Size = new System.Drawing.Size(100, 22);
            this.tbMail.TabIndex = 8;
            // 
            // btnAddProducator
            // 
            this.btnAddProducator.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnAddProducator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddProducator.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProducator.Location = new System.Drawing.Point(105, 267);
            this.btnAddProducator.Name = "btnAddProducator";
            this.btnAddProducator.Size = new System.Drawing.Size(81, 35);
            this.btnAddProducator.TabIndex = 9;
            this.btnAddProducator.Text = "ADD";
            this.btnAddProducator.UseVisualStyleBackColor = false;
            this.btnAddProducator.Click += new System.EventHandler(this.btnAddProducator_Click);
            // 
            // lbClose
            // 
            this.lbClose.AutoSize = true;
            this.lbClose.Location = new System.Drawing.Point(254, 320);
            this.lbClose.Name = "lbClose";
            this.lbClose.Size = new System.Drawing.Size(43, 17);
            this.lbClose.TabIndex = 10;
            this.lbClose.Text = "Close";
            this.lbClose.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbClose_MouseClick);
            // 
            // AdaugaProducator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(309, 346);
            this.Controls.Add(this.lbClose);
            this.Controls.Add(this.btnAddProducator);
            this.Controls.Add(this.tbMail);
            this.Controls.Add(this.tbCodFiscal);
            this.Controls.Add(this.tbTara);
            this.Controls.Add(this.tbNume);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbMail);
            this.Controls.Add(this.lbCodFiscal);
            this.Controls.Add(this.lbTara);
            this.Controls.Add(this.lbNume);
            this.Name = "AdaugaProducator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdaugaProducator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbNume;
        private System.Windows.Forms.Label lbTara;
        private System.Windows.Forms.Label lbCodFiscal;
        private System.Windows.Forms.Label lbMail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbNume;
        private System.Windows.Forms.TextBox tbTara;
        private System.Windows.Forms.TextBox tbCodFiscal;
        private System.Windows.Forms.TextBox tbMail;
        private System.Windows.Forms.Button btnAddProducator;
        private System.Windows.Forms.Label lbClose;
    }
}