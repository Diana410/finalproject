﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LicentaFINAL
{
    public partial class AdaugaProducator : Form
    {
        public AdaugaProducator()
        {
            InitializeComponent();
            populateProducatori();
        }


        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Chira\Documents\ApolloCare.mdf;Integrated Security=True;Connect Timeout=30");


        private void populateProducatori()
        {
            Con.Open();
            string query = "select * from ProducatoriTbl";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            //  SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            DataTable dt = new DataTable();
            sda.Fill(dt);

            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    cbProducator.Items.Add(dt.Rows[i]["Nume"]);
            //}

            Con.Close();
        }

        private void Clear()
        {
            tbNume.Text = "";
            tbTara.Text = "";
            tbCodFiscal.Text = "";
            tbMail.Text = "";
           
        }

        private void lbClose_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void btnAddProducator_Click(object sender, EventArgs e)
        {
            if (tbNume.Text == "" )
            {
                MessageBox.Show("Lipsesc informatii.");
            }
            else
            {
                Con.Open();
                SqlCommand cmd4 = new SqlCommand("insert into ProducatoriTbl values('" + tbNume.Text + " ','" + tbTara.Text + "', '" + tbCodFiscal.Text + "','" + tbMail.Text + "')", Con);
                cmd4.ExecuteNonQuery();

                Con.Close();
                Clear();
                
            }
            


        }
    }
}
