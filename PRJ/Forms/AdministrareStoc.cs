﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LicentaFINAL.Forms
{
    public partial class AdministrareStoc : Form
    {
        public AdministrareStoc()
        {
            InitializeComponent();
            populateProducatori();
            populate();
            populateSituatie();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Chira\Documents\ApolloCare.mdf;Integrated Security=True;Connect Timeout=30");

        private void populateProducatori()
        {
            Con.Open();
            string query = "select * from ProducatoriTbl";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            var ds = new DataSet();
            DataTable dt = new DataTable();
            sda.Fill(dt);

            for(int i = 0; i < dt.Rows.Count; i++)
            {
                cbProducator.Items.Add(dt.Rows[i]["Nume"]);
            }

            Con.Close();
        }

        private void populate()
        {
            Con.Open();
            string query = "select*from TranzactieTbl";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            TranzactiiDGV.DataSource = ds.Tables[0];
            Con.Close();
        }

        private void populateSituatie()
        {
            Con.Open();
           // string query = "select Medicament, Producator, Distribuitor, dataExpirare, sum(Cantitate) as CantitateTotala from StocUnitarTbl group by Medicament, Producator, Distribuitor, dataExpirare";
            string query = "select Medicament, Producator, dataExpirare, ValoareUnitara, CantitateTotala from SituatieStoc";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
          
            Con.Close();
        }

        private void Clear()
        {
            cbTip.SelectedIndex = -1;
            tbMedicament.Text = "";
            cbProducator.SelectedIndex = -1;
            cbDistribuitor.SelectedIndex = -1;
            tbLot.Text = "";
            tbSerie.Text = "";
            tbCantitate.Text = "";
            tbValoareUnitara.Text = "";
            tbId.Text = "";
        }

        private void AdministrareStoc_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'apolloCareDataSet.ProducatoriTbl' table. You can move, or remove it, as needed.
          //  this.producatoriTblTableAdapter.Fill(this.apolloCareDataSet.ProducatoriTbl);

        }

        private void lbProducator_Click(object sender, EventArgs e)
        {

        }

        private void cbProducator_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbProducator_Click_1(object sender, EventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {

            Clear();
        }

        private void btnAdaugaProducator_Click(object sender, EventArgs e)
        {
            AdaugaProducator adP = new AdaugaProducator();
            adP.ShowDialog();
           
        }

        private void apolloCareDataSetBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void btnADD_Click(object sender, EventArgs e)
        {
            if (cbTip.SelectedIndex == -1 || tbMedicament.Text == "" || cbProducator.SelectedIndex == -1 || cbDistribuitor.SelectedIndex == -1 || tbLot.Text == "" || tbSerie.Text == "" || tbCantitate.Text == "" || tbValoareUnitara.Text == "" || dtDataExpirare.Value.Date == DateTime.Now || dtDataIN.Value.Date == null || dtDataOUT.Value.Date == null)
            {
                MessageBox.Show("Lipsesc informatii.");
            }
            else
            {

                try
                {
                    Con.Open();

                    int stocExistent = 0;
                    SqlCommand cmdExist = new SqlCommand("select CantitateTotala from SituatieStoc where  Medicament=@Medicament and Producator=@Prod", Con);
                    cmdExist.Parameters.AddWithValue("@Medicament", tbMedicament.Text);
                    cmdExist.Parameters.AddWithValue("@Prod", cbProducator.Text);
                    stocExistent = Convert.ToInt32(cmdExist.ExecuteScalar());


                    if (cbTip.Text == "IN")
                    {
                        SqlCommand cmd2 = new SqlCommand("insert into TranzactieTbl values('" + cbTip.Text + " ','" + tbMedicament.Text + "', '" + cbProducator.Text + "','" + cbDistribuitor.Text + "','" + tbLot.Text + "','" + tbSerie.Text + "','" + tbCantitate.Text + "','" + tbValoareUnitara.Text + "','" + dtDataExpirare.Value.Date + "','" + dtDataIN.Value.Date + "','" + dtDataOUT.CustomFormat + "')", Con);
                        cmd2.ExecuteNonQuery();

                    }
                    else
                    {
                        if (cbTip.Text == "OUT")
                        {
                            if (stocExistent < Convert.ToInt32(tbCantitate.Text))
                            {
                                MessageBox.Show("Nu exista stoc disponibil!");
                            }
                            else
                            {
                                SqlCommand cmd3 = new SqlCommand("insert into TranzactieTbl values('" + cbTip.Text + " ','" + tbMedicament.Text + "', '" + cbProducator.Text + "','" + cbDistribuitor.Text + "','" + tbLot.Text + "','" + tbSerie.Text + "','" + tbCantitate.Text + "','" + tbValoareUnitara.Text + "','" + dtDataExpirare.Value.Date + "','" + dtDataIN.CustomFormat + "','" + dtDataOUT.Value.Date + "')", Con);
                                cmd3.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            SqlCommand cmd = new SqlCommand("insert into TranzactieTbl values('" + cbTip.Text + " ','" + tbMedicament.Text + "', '" + cbProducator.Text + "','" + cbDistribuitor.Text + "','" + tbLot.Text + "','" + tbSerie.Text + "','" + tbCantitate.Text + "','" + tbValoareUnitara.Text + "','" + dtDataExpirare.Value.Date + "','" + dtDataIN.Value.Date + "','" + dtDataOUT.Value.Date + "')", Con);
                            cmd.ExecuteNonQuery();
                        }
                    }

                   
                    int cantitate = Convert.ToInt32(tbCantitate.Text);
                   

                    while (cantitate != 0 && cbTip.Text == "IN")
                    {
                        int mID = 0;
                        SqlCommand cmd4 = new SqlCommand("select max(Id) as maxId from TranzactieTbl", Con);
                        mID = Convert.ToInt32(cmd4.ExecuteScalar());
                        SqlCommand cmdStoc = new SqlCommand("insert into StocUnitarTbl values('" + mID + " ','" + 0 + " ','" + tbMedicament.Text + "', '" + cbProducator.Text + "','" + cbDistribuitor.Text + "','" + tbLot.Text + "','" + tbSerie.Text + "','" + 1 + "','" + tbValoareUnitara.Text + "','" + dtDataExpirare.Value.Date + "','" + dtDataIN.Value.Date + "','" + null + "','" + 0 + "','" + null + "')", Con);
                       
                        cantitate--;
                        cmdStoc.ExecuteNonQuery();
                    }


                    while (cantitate != 0 && cbTip.Text == "OUT" && stocExistent>=Convert.ToInt32(tbCantitate.Text))
                    //while (cantitate != 0 && cbTip.Text == "OUT" )
                    {
                        int mID2 = 0;
                        SqlCommand cmd5 = new SqlCommand("select max(Id) as maxId from TranzactieTbl", Con);
                        mID2 = Convert.ToInt32(cmd5.ExecuteScalar());

                  
                        SqlCommand cmdUpd = new SqlCommand("update StocUnitarTbl set dataOUT=@dataOUT , idTranzactieOUT=@idOUT , NrZileInStoc=@nr, ValoareOUT=@ValoareOUT  where Medicament=@Medicament and dataOUT=@data and dataExpirare=@dataExpirare and Serie=@Serie and Lot=@Lot and Id = ( select min(Id) from StocUnitarTbl where dataExpirare=@dataExpirare and Serie=@Serie and Lot=@Lot and Medicament=@Medicament and dataOUT=@data )", Con);
                        cmdUpd.Parameters.AddWithValue("@dataOUT", dtDataOUT.Value);
                        cmdUpd.Parameters.AddWithValue("@dataExpirare", dtDataExpirare.Value);
                        cmdUpd.Parameters.AddWithValue("@Medicament", tbMedicament.Text);
                        cmdUpd.Parameters.AddWithValue("@Cantitate", tbCantitate.Text);
                        cmdUpd.Parameters.AddWithValue("@nr", (dtDataOUT.Value - dtDataIN.Value).Days);

                        cmdUpd.Parameters.AddWithValue("@data", "01/01/1900");

                        cmdUpd.Parameters.AddWithValue("@ValoareOUT", tbValoareUnitara.Text);
                        cmdUpd.Parameters.AddWithValue("@Serie", tbSerie.Text);
                        cmdUpd.Parameters.AddWithValue("@Lot", tbLot.Text);
                        cmdUpd.Parameters.AddWithValue("@idOUT", mID2);

                        cantitate--;
                        cmdUpd.ExecuteNonQuery();

                    }


                    Con.Close();
                    populate();

                    populateSituatie();
                    Clear();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                }
            }
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = TranzactiiDGV.DataSource;
            bs.Filter = "Medicament like '%" + tbSearch.Text + "%'";
            TranzactiiDGV.DataSource = bs;
        }

        private void btnDelete_Click(object sender, EventArgs e) 
        {
            //if (cbTip.SelectedIndex == -1 || tbMedicament.Text == "" || cbProducator.SelectedIndex == -1 || cbDistribuitor.SelectedIndex == -1 || tbLot.Text == "" || tbSerie.Text == "" || tbCantitate.Text == "" || tbValoareUnitara.Text == "" || dtDataExpirare.Value.Date == DateTime.Now || dtDataIN.Value.Date == null || dtDataOUT.Value.Date == null)
            if (tbId.Text == "" )
            {
                MessageBox.Show("Lipsesc informatii.");
            }
            //if (Key == 0)
            //{
            //    MessageBox.Show("Selecteaza produs ca sa fie sters.");
            //}
            else
            {
                try
                {
                    Con.Open();

                    SqlCommand cmdDel = new SqlCommand("delete from TranzactieTbl where Id=@Id ", Con);
                    cmdDel.Parameters.AddWithValue("@Id", tbId.Text);
                    cmdDel.ExecuteNonQuery();
                   

                    int cantitate = Convert.ToInt32(tbCantitate.Text);
                    int tipIN = cbTip.SelectedIndex;

                            SqlCommand cmdStoc2 = new SqlCommand("delete from StocUnitarTbl where idTranzactieIN=@IdIN ", Con);
                           
                            cmdStoc2.Parameters.AddWithValue("@Medicament", tbMedicament.Text);
                            cmdStoc2.Parameters.AddWithValue("@Cantitate", tbCantitate.Text);
                            cmdStoc2.Parameters.AddWithValue("@Serie", tbSerie.Text);
                            cmdStoc2.Parameters.AddWithValue("@Lot", tbLot.Text);     
                            cmdStoc2.Parameters.AddWithValue("@data", "01/01/1900");
                            cmdStoc2.Parameters.AddWithValue("@IdIN", tbId.Text);
                            cantitate--;
                            cmdStoc2.ExecuteNonQuery();


                    Con.Close();
                    populate();
                    populateSituatie();
                    Clear();
                }


                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                }
            }
        }

        int Key = 0;

        private void TranzactiiDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            tbId.Text= TranzactiiDGV.SelectedRows[0].Cells[0].Value.ToString();

            cbTip.Text = TranzactiiDGV.SelectedRows[0].Cells[1].Value.ToString();
            tbMedicament.Text = TranzactiiDGV.SelectedRows[0].Cells[2].Value.ToString();
            cbProducator.Text = TranzactiiDGV.SelectedRows[0].Cells[3].Value.ToString();
            cbDistribuitor.Text = TranzactiiDGV.SelectedRows[0].Cells[4].Value.ToString();
            tbLot.Text = TranzactiiDGV.SelectedRows[0].Cells[5].Value.ToString();
            tbSerie.Text = TranzactiiDGV.SelectedRows[0].Cells[6].Value.ToString();
            tbCantitate.Text = TranzactiiDGV.SelectedRows[0].Cells[7].Value.ToString();
            tbValoareUnitara.Text = TranzactiiDGV.SelectedRows[0].Cells[8].Value.ToString();
            dtDataExpirare.Value = Convert.ToDateTime(TranzactiiDGV.SelectedRows[0].Cells[9].Value);
            dtDataIN.Value = Convert.ToDateTime(TranzactiiDGV.SelectedRows[0].Cells[10].Value);
            dtDataOUT.Value = Convert.ToDateTime(TranzactiiDGV.SelectedRows[0].Cells[11].Value);

            if (tbMedicament.Text == "")
            {
                Key = 0;

            }
            else
            {
                Key = Convert.ToInt32(TranzactiiDGV.SelectedRows[0].Cells[0].Value.ToString());
            }
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = TranzactiiDGV.DataSource;
            bs.Filter = "Lot like '%" + tb2.Text + "%'";
            TranzactiiDGV.DataSource = bs;
        }
    }
}
