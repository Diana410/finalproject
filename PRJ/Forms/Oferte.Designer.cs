﻿namespace LicentaFINAL.Forms
{
    partial class Oferte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbFile = new System.Windows.Forms.Label();
            this.lbSheet = new System.Windows.Forms.Label();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.cboSheet = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.FurnizoriDGV = new System.Windows.Forms.DataGridView();
            this.dgvNrZile = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSearch2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.FurnizoriDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNrZile)).BeginInit();
            this.SuspendLayout();
            // 
            // lbFile
            // 
            this.lbFile.AutoSize = true;
            this.lbFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFile.Location = new System.Drawing.Point(15, 261);
            this.lbFile.Name = "lbFile";
            this.lbFile.Size = new System.Drawing.Size(103, 25);
            this.lbFile.TabIndex = 1;
            this.lbFile.Text = "File name:";
            // 
            // lbSheet
            // 
            this.lbSheet.AutoSize = true;
            this.lbSheet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSheet.Location = new System.Drawing.Point(15, 308);
            this.lbSheet.Name = "lbSheet";
            this.lbSheet.Size = new System.Drawing.Size(75, 25);
            this.lbSheet.TabIndex = 2;
            this.lbSheet.Text = "Sheet: ";
            // 
            // txtFilename
            // 
            this.txtFilename.Location = new System.Drawing.Point(120, 261);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.ReadOnly = true;
            this.txtFilename.Size = new System.Drawing.Size(353, 22);
            this.txtFilename.TabIndex = 3;
            // 
            // cboSheet
            // 
            this.cboSheet.FormattingEnabled = true;
            this.cboSheet.Location = new System.Drawing.Point(120, 308);
            this.cboSheet.Name = "cboSheet";
            this.cboSheet.Size = new System.Drawing.Size(288, 24);
            this.cboSheet.TabIndex = 4;
            this.cboSheet.SelectedIndexChanged += new System.EventHandler(this.cboSheet_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Search: ";
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(117, 120);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(342, 22);
            this.tbSearch.TabIndex = 6;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // btnBrowse
            // 
            this.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(489, 251);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(56, 35);
            this.btnBrowse.TabIndex = 7;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // FurnizoriDGV
            // 
            this.FurnizoriDGV.AllowUserToOrderColumns = true;
            this.FurnizoriDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.FurnizoriDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FurnizoriDGV.Location = new System.Drawing.Point(21, 356);
            this.FurnizoriDGV.Name = "FurnizoriDGV";
            this.FurnizoriDGV.RowHeadersWidth = 51;
            this.FurnizoriDGV.RowTemplate.Height = 24;
            this.FurnizoriDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.FurnizoriDGV.Size = new System.Drawing.Size(1248, 356);
            this.FurnizoriDGV.TabIndex = 8;
            // 
            // dgvNrZile
            // 
            this.dgvNrZile.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvNrZile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNrZile.Location = new System.Drawing.Point(578, 24);
            this.dgvNrZile.Name = "dgvNrZile";
            this.dgvNrZile.RowHeadersWidth = 51;
            this.dgvNrZile.RowTemplate.Height = 24;
            this.dgvNrZile.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNrZile.Size = new System.Drawing.Size(681, 307);
            this.dgvNrZile.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Algerian", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(404, 45);
            this.label1.TabIndex = 10;
            this.label1.Text = "Viteza de rotatie:";
            // 
            // tbSearch2
            // 
            this.tbSearch2.Location = new System.Drawing.Point(217, 160);
            this.tbSearch2.Name = "tbSearch2";
            this.tbSearch2.Size = new System.Drawing.Size(242, 22);
            this.tbSearch2.TabIndex = 12;
            this.tbSearch2.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Dupa Producator:";
            // 
            // Oferte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 749);
            this.Controls.Add(this.tbSearch2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvNrZile);
            this.Controls.Add(this.FurnizoriDGV);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboSheet);
            this.Controls.Add(this.txtFilename);
            this.Controls.Add(this.lbSheet);
            this.Controls.Add(this.lbFile);
            this.Name = "Oferte";
            this.Text = "Oferte";
            ((System.ComponentModel.ISupportInitialize)(this.FurnizoriDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNrZile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbFile;
        private System.Windows.Forms.Label lbSheet;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.ComboBox cboSheet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.DataGridView FurnizoriDGV;
        private System.Windows.Forms.DataGridView dgvNrZile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbSearch2;
        private System.Windows.Forms.Label label3;
    }
}