﻿namespace LicentaFINAL.Forms
{
    partial class AdministrareStoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnADD = new System.Windows.Forms.Button();
            this.tbMedicament = new System.Windows.Forms.TextBox();
            this.cbProducator = new System.Windows.Forms.ComboBox();
            this.apolloCareDataSet = new LicentaFINAL.ApolloCareDataSet();
            this.producatoriTblBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.producatoriTblTableAdapter = new LicentaFINAL.ApolloCareDataSetTableAdapters.ProducatoriTblTableAdapter();
            this.cbTip = new System.Windows.Forms.ComboBox();
            this.lbTip = new System.Windows.Forms.Label();
            this.lbMedicament = new System.Windows.Forms.Label();
            this.lbProducator = new System.Windows.Forms.Label();
            this.lbDistribuitor = new System.Windows.Forms.Label();
            this.lbLot = new System.Windows.Forms.Label();
            this.lbSerie = new System.Windows.Forms.Label();
            this.tbLot = new System.Windows.Forms.TextBox();
            this.cbDistribuitor = new System.Windows.Forms.ComboBox();
            this.tbSerie = new System.Windows.Forms.TextBox();
            this.tbCantitate = new System.Windows.Forms.TextBox();
            this.tbValoareUnitara = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbValoareUnitara = new System.Windows.Forms.Label();
            this.lbDataExpirare = new System.Windows.Forms.Label();
            this.lbDataIN = new System.Windows.Forms.Label();
            this.lbDataOUT = new System.Windows.Forms.Label();
            this.dtDataExpirare = new System.Windows.Forms.DateTimePicker();
            this.dtDataIN = new System.Windows.Forms.DateTimePicker();
            this.dtDataOUT = new System.Windows.Forms.DateTimePicker();
            this.TranzactiiDGV = new System.Windows.Forms.DataGridView();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lbSearch = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdaugaProducator = new System.Windows.Forms.Button();
            this.apolloCareDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.apolloCareDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.producatoriTblBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TranzactiiDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apolloCareDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnADD
            // 
            this.btnADD.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnADD.FlatAppearance.BorderSize = 0;
            this.btnADD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnADD.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnADD.ForeColor = System.Drawing.Color.Azure;
            this.btnADD.Location = new System.Drawing.Point(1138, 127);
            this.btnADD.Name = "btnADD";
            this.btnADD.Size = new System.Drawing.Size(121, 37);
            this.btnADD.TabIndex = 0;
            this.btnADD.Text = "Creaza";
            this.btnADD.UseVisualStyleBackColor = false;
            this.btnADD.Click += new System.EventHandler(this.btnADD_Click);
            // 
            // tbMedicament
            // 
            this.tbMedicament.Location = new System.Drawing.Point(221, 60);
            this.tbMedicament.Name = "tbMedicament";
            this.tbMedicament.Size = new System.Drawing.Size(138, 22);
            this.tbMedicament.TabIndex = 1;
            this.tbMedicament.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // cbProducator
            // 
            this.cbProducator.FormattingEnabled = true;
            this.cbProducator.Location = new System.Drawing.Point(394, 58);
            this.cbProducator.Name = "cbProducator";
            this.cbProducator.Size = new System.Drawing.Size(156, 24);
            this.cbProducator.TabIndex = 3;
            this.cbProducator.SelectedIndexChanged += new System.EventHandler(this.cbProducator_SelectedIndexChanged);
            // 
            // apolloCareDataSet
            // 
            this.apolloCareDataSet.DataSetName = "ApolloCareDataSet";
            this.apolloCareDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // producatoriTblBindingSource
            // 
            this.producatoriTblBindingSource.DataMember = "ProducatoriTbl";
            this.producatoriTblBindingSource.DataSource = this.apolloCareDataSet;
            // 
            // producatoriTblTableAdapter
            // 
            this.producatoriTblTableAdapter.ClearBeforeFill = true;
            // 
            // cbTip
            // 
            this.cbTip.FormattingEnabled = true;
            this.cbTip.Items.AddRange(new object[] {
            "IN",
            "OUT"});
            this.cbTip.Location = new System.Drawing.Point(48, 58);
            this.cbTip.Name = "cbTip";
            this.cbTip.Size = new System.Drawing.Size(117, 24);
            this.cbTip.TabIndex = 4;
            // 
            // lbTip
            // 
            this.lbTip.AutoSize = true;
            this.lbTip.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTip.Location = new System.Drawing.Point(44, 30);
            this.lbTip.Name = "lbTip";
            this.lbTip.Size = new System.Drawing.Size(121, 20);
            this.lbTip.TabIndex = 5;
            this.lbTip.Text = "Tip Tranzactie:";
            // 
            // lbMedicament
            // 
            this.lbMedicament.AutoSize = true;
            this.lbMedicament.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMedicament.Location = new System.Drawing.Point(217, 30);
            this.lbMedicament.Name = "lbMedicament";
            this.lbMedicament.Size = new System.Drawing.Size(105, 20);
            this.lbMedicament.TabIndex = 6;
            this.lbMedicament.Text = "Medicament:";
            // 
            // lbProducator
            // 
            this.lbProducator.AutoSize = true;
            this.lbProducator.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProducator.Location = new System.Drawing.Point(390, 30);
            this.lbProducator.Name = "lbProducator";
            this.lbProducator.Size = new System.Drawing.Size(96, 20);
            this.lbProducator.TabIndex = 7;
            this.lbProducator.Text = "Producator:";
            this.lbProducator.Click += new System.EventHandler(this.lbProducator_Click_1);
            // 
            // lbDistribuitor
            // 
            this.lbDistribuitor.AutoSize = true;
            this.lbDistribuitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDistribuitor.Location = new System.Drawing.Point(630, 30);
            this.lbDistribuitor.Name = "lbDistribuitor";
            this.lbDistribuitor.Size = new System.Drawing.Size(97, 20);
            this.lbDistribuitor.TabIndex = 8;
            this.lbDistribuitor.Text = "Distribuitor:";
            // 
            // lbLot
            // 
            this.lbLot.AutoSize = true;
            this.lbLot.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLot.Location = new System.Drawing.Point(817, 30);
            this.lbLot.Name = "lbLot";
            this.lbLot.Size = new System.Drawing.Size(43, 20);
            this.lbLot.TabIndex = 9;
            this.lbLot.Text = "Lot: ";
            // 
            // lbSerie
            // 
            this.lbSerie.AutoSize = true;
            this.lbSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSerie.Location = new System.Drawing.Point(976, 30);
            this.lbSerie.Name = "lbSerie";
            this.lbSerie.Size = new System.Drawing.Size(58, 20);
            this.lbSerie.TabIndex = 10;
            this.lbSerie.Text = "Serie: ";
            // 
            // tbLot
            // 
            this.tbLot.Location = new System.Drawing.Point(821, 61);
            this.tbLot.Name = "tbLot";
            this.tbLot.Size = new System.Drawing.Size(122, 22);
            this.tbLot.TabIndex = 11;
            // 
            // cbDistribuitor
            // 
            this.cbDistribuitor.FormattingEnabled = true;
            this.cbDistribuitor.Items.AddRange(new object[] {
            "Distribuitor1",
            "Distribuitor2",
            "Distribuitor3",
            "Distribuitor4"});
            this.cbDistribuitor.Location = new System.Drawing.Point(634, 59);
            this.cbDistribuitor.Name = "cbDistribuitor";
            this.cbDistribuitor.Size = new System.Drawing.Size(149, 24);
            this.cbDistribuitor.TabIndex = 12;
            // 
            // tbSerie
            // 
            this.tbSerie.Location = new System.Drawing.Point(980, 60);
            this.tbSerie.Name = "tbSerie";
            this.tbSerie.Size = new System.Drawing.Size(122, 22);
            this.tbSerie.TabIndex = 13;
            // 
            // tbCantitate
            // 
            this.tbCantitate.Location = new System.Drawing.Point(1137, 60);
            this.tbCantitate.Name = "tbCantitate";
            this.tbCantitate.Size = new System.Drawing.Size(122, 22);
            this.tbCantitate.TabIndex = 14;
            // 
            // tbValoareUnitara
            // 
            this.tbValoareUnitara.Location = new System.Drawing.Point(48, 142);
            this.tbValoareUnitara.Name = "tbValoareUnitara";
            this.tbValoareUnitara.Size = new System.Drawing.Size(122, 22);
            this.tbValoareUnitara.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1133, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 16;
            this.label1.Text = "Cantitate: ";
            // 
            // lbValoareUnitara
            // 
            this.lbValoareUnitara.AutoSize = true;
            this.lbValoareUnitara.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbValoareUnitara.Location = new System.Drawing.Point(44, 109);
            this.lbValoareUnitara.Name = "lbValoareUnitara";
            this.lbValoareUnitara.Size = new System.Drawing.Size(130, 20);
            this.lbValoareUnitara.TabIndex = 17;
            this.lbValoareUnitara.Text = "Valoare Unitara:";
            // 
            // lbDataExpirare
            // 
            this.lbDataExpirare.AutoSize = true;
            this.lbDataExpirare.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDataExpirare.Location = new System.Drawing.Point(348, 112);
            this.lbDataExpirare.Name = "lbDataExpirare";
            this.lbDataExpirare.Size = new System.Drawing.Size(120, 20);
            this.lbDataExpirare.TabIndex = 18;
            this.lbDataExpirare.Text = "Data expirare: ";
            // 
            // lbDataIN
            // 
            this.lbDataIN.AutoSize = true;
            this.lbDataIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDataIN.Location = new System.Drawing.Point(580, 112);
            this.lbDataIN.Name = "lbDataIN";
            this.lbDataIN.Size = new System.Drawing.Size(76, 20);
            this.lbDataIN.TabIndex = 19;
            this.lbDataIN.Text = "Data IN: ";
            // 
            // lbDataOUT
            // 
            this.lbDataOUT.AutoSize = true;
            this.lbDataOUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDataOUT.Location = new System.Drawing.Point(801, 112);
            this.lbDataOUT.Name = "lbDataOUT";
            this.lbDataOUT.Size = new System.Drawing.Size(95, 20);
            this.lbDataOUT.TabIndex = 20;
            this.lbDataOUT.Text = "Data OUT: ";
            // 
            // dtDataExpirare
            // 
            this.dtDataExpirare.Location = new System.Drawing.Point(352, 142);
            this.dtDataExpirare.Name = "dtDataExpirare";
            this.dtDataExpirare.Size = new System.Drawing.Size(157, 22);
            this.dtDataExpirare.TabIndex = 21;
            // 
            // dtDataIN
            // 
            this.dtDataIN.Location = new System.Drawing.Point(584, 142);
            this.dtDataIN.Name = "dtDataIN";
            this.dtDataIN.Size = new System.Drawing.Size(154, 22);
            this.dtDataIN.TabIndex = 22;
            // 
            // dtDataOUT
            // 
            this.dtDataOUT.Location = new System.Drawing.Point(805, 142);
            this.dtDataOUT.Name = "dtDataOUT";
            this.dtDataOUT.Size = new System.Drawing.Size(157, 22);
            this.dtDataOUT.TabIndex = 23;
            // 
            // TranzactiiDGV
            // 
            this.TranzactiiDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TranzactiiDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TranzactiiDGV.Location = new System.Drawing.Point(48, 262);
            this.TranzactiiDGV.Name = "TranzactiiDGV";
            this.TranzactiiDGV.RowHeadersWidth = 51;
            this.TranzactiiDGV.RowTemplate.Height = 24;
            this.TranzactiiDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TranzactiiDGV.Size = new System.Drawing.Size(1211, 460);
            this.TranzactiiDGV.TabIndex = 24;
            this.TranzactiiDGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TranzactiiDGV_CellContentClick);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(144, 227);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(297, 22);
            this.tbSearch.TabIndex = 25;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // lbSearch
            // 
            this.lbSearch.AutoSize = true;
            this.lbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSearch.Location = new System.Drawing.Point(44, 224);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(94, 25);
            this.lbSearch.TabIndex = 26;
            this.lbSearch.Text = "Search: ";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Times New Roman", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.Azure;
            this.btnClear.Location = new System.Drawing.Point(1174, 224);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 28);
            this.btnClear.TabIndex = 27;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Times New Roman", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Azure;
            this.btnDelete.Location = new System.Drawing.Point(1073, 224);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 28);
            this.btnDelete.TabIndex = 28;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdaugaProducator
            // 
            this.btnAdaugaProducator.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAdaugaProducator.FlatAppearance.BorderSize = 0;
            this.btnAdaugaProducator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdaugaProducator.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdaugaProducator.ForeColor = System.Drawing.Color.Azure;
            this.btnAdaugaProducator.Location = new System.Drawing.Point(556, 58);
            this.btnAdaugaProducator.Name = "btnAdaugaProducator";
            this.btnAdaugaProducator.Size = new System.Drawing.Size(35, 25);
            this.btnAdaugaProducator.TabIndex = 29;
            this.btnAdaugaProducator.Text = "+";
            this.btnAdaugaProducator.UseVisualStyleBackColor = false;
            this.btnAdaugaProducator.Click += new System.EventHandler(this.btnAdaugaProducator_Click);
            // 
            // apolloCareDataSetBindingSource
            // 
            this.apolloCareDataSetBindingSource.DataSource = this.apolloCareDataSet;
            this.apolloCareDataSetBindingSource.Position = 0;
            this.apolloCareDataSetBindingSource.CurrentChanged += new System.EventHandler(this.apolloCareDataSetBindingSource_CurrentChanged);
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(1002, 142);
            this.tbId.Name = "tbId";
            this.tbId.ReadOnly = true;
            this.tbId.Size = new System.Drawing.Size(100, 22);
            this.tbId.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(522, 227);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 25);
            this.label2.TabIndex = 33;
            this.label2.Text = "Lot:";
            // 
            // tb2
            // 
            this.tb2.Location = new System.Drawing.Point(634, 228);
            this.tb2.Name = "tb2";
            this.tb2.Size = new System.Drawing.Size(157, 22);
            this.tb2.TabIndex = 32;
            this.tb2.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // AdministrareStoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 749);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb2);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.btnAdaugaProducator);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lbSearch);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.TranzactiiDGV);
            this.Controls.Add(this.dtDataOUT);
            this.Controls.Add(this.dtDataIN);
            this.Controls.Add(this.dtDataExpirare);
            this.Controls.Add(this.lbDataOUT);
            this.Controls.Add(this.lbDataIN);
            this.Controls.Add(this.lbDataExpirare);
            this.Controls.Add(this.lbValoareUnitara);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbValoareUnitara);
            this.Controls.Add(this.tbCantitate);
            this.Controls.Add(this.tbSerie);
            this.Controls.Add(this.cbDistribuitor);
            this.Controls.Add(this.tbLot);
            this.Controls.Add(this.lbSerie);
            this.Controls.Add(this.lbLot);
            this.Controls.Add(this.lbDistribuitor);
            this.Controls.Add(this.lbProducator);
            this.Controls.Add(this.lbMedicament);
            this.Controls.Add(this.lbTip);
            this.Controls.Add(this.cbTip);
            this.Controls.Add(this.cbProducator);
            this.Controls.Add(this.tbMedicament);
            this.Controls.Add(this.btnADD);
            this.Name = "AdministrareStoc";
            this.Text = "AdministrareStoc";
            this.Load += new System.EventHandler(this.AdministrareStoc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.apolloCareDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.producatoriTblBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TranzactiiDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apolloCareDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnADD;
        private System.Windows.Forms.TextBox tbMedicament;
        private System.Windows.Forms.ComboBox cbProducator;
        private ApolloCareDataSet apolloCareDataSet;
        private System.Windows.Forms.BindingSource producatoriTblBindingSource;
        private ApolloCareDataSetTableAdapters.ProducatoriTblTableAdapter producatoriTblTableAdapter;
        private System.Windows.Forms.ComboBox cbTip;
        private System.Windows.Forms.Label lbTip;
        private System.Windows.Forms.Label lbMedicament;
        private System.Windows.Forms.Label lbProducator;
        private System.Windows.Forms.Label lbDistribuitor;
        private System.Windows.Forms.Label lbLot;
        private System.Windows.Forms.Label lbSerie;
        private System.Windows.Forms.TextBox tbLot;
        private System.Windows.Forms.ComboBox cbDistribuitor;
        private System.Windows.Forms.TextBox tbSerie;
        private System.Windows.Forms.TextBox tbCantitate;
        private System.Windows.Forms.TextBox tbValoareUnitara;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbValoareUnitara;
        private System.Windows.Forms.Label lbDataExpirare;
        private System.Windows.Forms.Label lbDataIN;
        private System.Windows.Forms.Label lbDataOUT;
        private System.Windows.Forms.DateTimePicker dtDataExpirare;
        private System.Windows.Forms.DateTimePicker dtDataIN;
        private System.Windows.Forms.DateTimePicker dtDataOUT;
        private System.Windows.Forms.DataGridView TranzactiiDGV;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdaugaProducator;
        private System.Windows.Forms.BindingSource apolloCareDataSetBindingSource;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb2;
    }
}