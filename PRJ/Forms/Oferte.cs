﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelDataReader;
using System.Data.SqlClient;

namespace LicentaFINAL.Forms
{
    public partial class Oferte : Form
    {
        public Oferte()
        {
            InitializeComponent();
            populateRotatie();
        }

        DataTableCollection tableCollection;

        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Chira\Documents\ApolloCare.mdf;Integrated Security=True;Connect Timeout=30");

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook|*.xlsx" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    txtFilename.Text = openFileDialog.FileName;
                    using (var stream = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                            });
                            tableCollection = result.Tables;
                            cboSheet.Items.Clear();
                            foreach (DataTable table in tableCollection)
                                cboSheet.Items.Add(table.TableName);//add sheet to combobox
                        }
                    }
                }
            }
        }

        private void cboSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = tableCollection[cboSheet.SelectedItem.ToString()];
            FurnizoriDGV.DataSource = dt;
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = FurnizoriDGV.DataSource;
            bs.Filter = "Medicament like '%" + tbSearch.Text + "%'";
            FurnizoriDGV.DataSource = bs;

            BindingSource bs2 = new BindingSource();
            bs2.DataSource = dgvNrZile.DataSource;
            bs2.Filter = "Medicament like '%" + tbSearch.Text + "%'";
            dgvNrZile.DataSource = bs2;

        }


        private void populateRotatie()
        {
            Con.Open();
            string query = "select Medicament, Producator, CantitateInStoc,CantitateVanduta,NrZileInStoc from AggRotatie";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            dgvNrZile.DataSource = ds.Tables[0];

            dgvNrZile.Refresh();
            Con.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = FurnizoriDGV.DataSource;
            bs.Filter = "Producator like '%" + tbSearch2.Text + "%'";
            FurnizoriDGV.DataSource = bs;

            BindingSource bs2 = new BindingSource();
            bs2.DataSource = dgvNrZile.DataSource;
            bs2.Filter = "Producator like '%" + tbSearch2.Text + "%'";
            dgvNrZile.DataSource = bs2;

        }
    }
}
