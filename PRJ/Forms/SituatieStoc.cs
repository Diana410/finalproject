﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LicentaFINAL.Forms
{
    public partial class SituatieStoc : Form
    {
        public SituatieStoc()
        {
            InitializeComponent();
            populateSituatie();
            populateProducatori();
            populateSituatieVanzari();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Chira\Documents\ApolloCare.mdf;Integrated Security=True;Connect Timeout=30");

        private void populateSituatie()
        {
            Con.Open();
            // string query = "select Medicament, Producator, Distribuitor, dataExpirare, sum(Cantitate) as CantitateTotala from StocUnitarTbl group by Medicament, Producator, Distribuitor, dataExpirare";
            string query = "select Medicament, Producator, dataExpirare, ValoareUnitara, CantitateTotala from SituatieStoc";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            ViewSituatieStoc.DataSource = ds.Tables[0];

            ViewSituatieStoc.Refresh();
            Con.Close();
        }

        private void populateSituatieVanzari()
        {
            Con.Open();
            // string query = "select Medicament, Producator, Distribuitor, dataExpirare, sum(Cantitate) as CantitateTotala from StocUnitarTbl group by Medicament, Producator, Distribuitor, dataExpirare";
            string query = "select Medicament, Producator, DataVanzare, Cantitate, ValoareTotala from SituatieVanzari";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            ViewSituatieVanzari.DataSource = ds.Tables[0];

            ViewSituatieVanzari.Refresh();
            Con.Close();
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = ViewSituatieStoc.DataSource;
            bs.Filter = "Medicament like '%" + tbSearch.Text + "%'";
            ViewSituatieStoc.DataSource = bs;
        }

        private void populateProducatori()
        {
            Con.Open();
            string query = "select * from ProducatoriTbl";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            //  SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            DataTable dt = new DataTable();
            sda.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cbProducator.Items.Add(dt.Rows[i]["Nume"]);
                cbProducator2.Items.Add(dt.Rows[i]["Nume"]);

            }

            Con.Close();
        }

        private void cbProducator_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = ViewSituatieStoc.DataSource;
            bs.Filter = "Producator like '%" + cbProducator.Text + "%'";
            ViewSituatieStoc.DataSource = bs;
        }

        private void tbSearch2_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = ViewSituatieVanzari.DataSource;
            bs.Filter = "Medicament like '%" + tbSearch2.Text + "%'";
            ViewSituatieVanzari.DataSource = bs;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = ViewSituatieVanzari.DataSource;
            bs.Filter = "Producator like '%" + cbProducator2.Text + "%'";
            ViewSituatieVanzari.DataSource = bs;
        }
    }
}
