﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LicentaFINAL.Forms
{
    public partial class Rapoarte : Form
    {
        public Rapoarte()
        {
            InitializeComponent();
            //populateSituatie();
            populateSituatie2();
            populateSituatie3();
            populateDGVProd();
            populatePerioada();
            populateMedicamente();
            populateDupaMedicament();
            populateInZiua();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Chira\Documents\ApolloCare.mdf;Integrated Security=True;Connect Timeout=30");

        private void populateMedicamente()
        {
            Con.Open();
            string query = "select distinct Medicament from StocUnitarTbl";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            var ds = new DataSet();
            DataTable dt = new DataTable();
            sda.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cbMedicamente.Items.Add(dt.Rows[i]["Medicament"]);
            }

            Con.Close();
        }


        private void populateSituatie2()
        {

            try
            {
                Con.Open();

                SqlCommand comanda = new SqlCommand();
                comanda.Connection = Con;
                string query2 = "select Producator, NrMedicamente from GraficProdVanzari";
                comanda.CommandText = query2;
                SqlDataReader reader = comanda.ExecuteReader();

                while (reader.Read())
                {
                    chartPie1.Series["Series1"].Points.AddXY(reader["Producator"].ToString(), reader["NrMedicamente"]);
                }
                Con.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void populateSituatie3()
        {

            try
            {
                Con.Open();

                SqlCommand comanda = new SqlCommand();
                comanda.Connection = Con;
                string query2 = "select Producator, NrMedicamente from GraficProdStoc";
                comanda.CommandText = query2;
                SqlDataReader reader = comanda.ExecuteReader();

                while (reader.Read())
                {
                    chartPie2.Series["Series1"].Points.AddXY(reader["Producator"].ToString(), reader["NrMedicamente"]);
                }
                Con.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void populateDGVProd()
        {
            Con.Open();
            string query = "select Prod, CantitateVanduta, CantitateStoc from CantProducatori";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            dgvProducatori.DataSource = ds.Tables[0];

            dgvProducatori.Refresh();
            Con.Close();
        }


        private void populatePerioada()
        {
            foreach (var series in chart4.Series)
            {
                series.Points.Clear();
            }

            try
            {
                Con.Open();

                SqlCommand cmdDel = new SqlCommand("Select sum(Cantitate) as Cant, dataOUT from StocUnitarTbl where dataOUT between @DataDeLa and @DataPanaLa group by dataOUT", Con);
             
                cmdDel.Parameters.AddWithValue("@DataDeLa", deLa.Value.Date);
                cmdDel.Parameters.AddWithValue("@DataPanaLa", panaLa.Value.Date);
              
                SqlDataReader reader = cmdDel.ExecuteReader();

                while (reader.Read())
                {
                    chart4.Series["Series1"].Points.AddXY(reader["dataOUT"].ToString(), reader["Cant"]);
                }

                Con.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }



        private void populateDupaMedicament()
        {

            foreach (var series in chart4.Series)
            {
                series.Points.Clear();
            }

            try
            {
                Con.Open();

                SqlCommand cmdDel = new SqlCommand("Select Medicament, sum(Cantitate) as Cant, dataOUT from StocUnitarTbl where Medicament=@Med and dataOUT >= DATEADD(MONTH, -3, GETDATE()) group by dataOUT,Medicament", Con);
                
                cmdDel.Parameters.AddWithValue("@Med", cbMedicamente.Text);

                SqlDataReader reader = cmdDel.ExecuteReader();

                while (reader.Read())
                {
                    chart4.Series["Series1"].Points.AddXY(reader["dataOUT"].ToString(), reader["Cant"]);
                }

                Con.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void populateInZiua()
        {
            foreach (var series in chart4.Series)
            {
                series.Points.Clear();
            }

            try
            {
                Con.Open();

                SqlCommand cmdDel = new SqlCommand("Select Medicament, sum(Cantitate) as Cant, dataOUT from StocUnitarTbl where dataOUT= @DataOUT group by Medicament,dataOUT", Con);
                cmdDel.Parameters.AddWithValue("@DataOUT", inZiua.Value.Date);
               
                SqlDataReader reader = cmdDel.ExecuteReader();

                while (reader.Read())
                {
                    chart4.Series["Series1"].Points.AddXY(reader["Medicament"].ToString(), reader["Cant"]);
                }

                Con.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void panaLa_ValueChanged(object sender, EventArgs e)
        {
            populatePerioada();
        }

        private void cbMedicamente_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateDupaMedicament();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            chart4.Printing.PrintPreview();
        }

        private void inZiua_ValueChanged(object sender, EventArgs e)
        {
            populateInZiua();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        
    }
}
