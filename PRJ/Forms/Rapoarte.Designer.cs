﻿namespace LicentaFINAL.Forms
{
    partial class Rapoarte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label1 = new System.Windows.Forms.Label();
            this.chartPie1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lbProducatori = new System.Windows.Forms.Label();
            this.chartPie2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvProducatori = new System.Windows.Forms.DataGridView();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.deLa = new System.Windows.Forms.DateTimePicker();
            this.panaLa = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbMedicamente = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.inZiua = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartPie1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPie2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducatori)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(120, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 0;
            // 
            // chartPie1
            // 
            chartArea4.Name = "ChartArea1";
            this.chartPie1.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chartPie1.Legends.Add(legend4);
            this.chartPie1.Location = new System.Drawing.Point(38, 61);
            this.chartPie1.Name = "chartPie1";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chartPie1.Series.Add(series4);
            this.chartPie1.Size = new System.Drawing.Size(325, 198);
            this.chartPie1.TabIndex = 2;
            this.chartPie1.Text = "chart2";
            // 
            // lbProducatori
            // 
            this.lbProducatori.AutoSize = true;
            this.lbProducatori.Font = new System.Drawing.Font("Algerian", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProducatori.ForeColor = System.Drawing.Color.Indigo;
            this.lbProducatori.Location = new System.Drawing.Point(33, 9);
            this.lbProducatori.Name = "lbProducatori";
            this.lbProducatori.Size = new System.Drawing.Size(291, 45);
            this.lbProducatori.TabIndex = 3;
            this.lbProducatori.Text = "PRODUCATORI:";
            // 
            // chartPie2
            // 
            chartArea5.Name = "ChartArea1";
            this.chartPie2.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.chartPie2.Legends.Add(legend5);
            this.chartPie2.Location = new System.Drawing.Point(391, 61);
            this.chartPie2.Name = "chartPie2";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.chartPie2.Series.Add(series5);
            this.chartPie2.Size = new System.Drawing.Size(325, 198);
            this.chartPie2.TabIndex = 4;
            this.chartPie2.Text = "chart2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gloucester MT Extra Condensed", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(146, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 32);
            this.label2.TabIndex = 5;
            this.label2.Text = "Vandut";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Gloucester MT Extra Condensed", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(507, 262);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 32);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cumparat";
            // 
            // dgvProducatori
            // 
            this.dgvProducatori.AllowUserToOrderColumns = true;
            this.dgvProducatori.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProducatori.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProducatori.Location = new System.Drawing.Point(759, 61);
            this.dgvProducatori.Name = "dgvProducatori";
            this.dgvProducatori.RowHeadersWidth = 51;
            this.dgvProducatori.RowTemplate.Height = 24;
            this.dgvProducatori.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProducatori.Size = new System.Drawing.Size(508, 198);
            this.dgvProducatori.TabIndex = 7;
            // 
            // chart4
            // 
            chartArea6.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chart4.Legends.Add(legend6);
            this.chart4.Location = new System.Drawing.Point(298, 352);
            this.chart4.Name = "chart4";
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.chart4.Series.Add(series6);
            this.chart4.Size = new System.Drawing.Size(972, 375);
            this.chart4.TabIndex = 8;
            this.chart4.Text = "chart2";
            // 
            // deLa
            // 
            this.deLa.Location = new System.Drawing.Point(30, 432);
            this.deLa.Name = "deLa";
            this.deLa.Size = new System.Drawing.Size(200, 22);
            this.deLa.TabIndex = 9;
            // 
            // panaLa
            // 
            this.panaLa.Location = new System.Drawing.Point(30, 506);
            this.panaLa.Name = "panaLa";
            this.panaLa.Size = new System.Drawing.Size(200, 22);
            this.panaLa.TabIndex = 10;
            this.panaLa.ValueChanged += new System.EventHandler(this.panaLa_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Agency FB", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(35, 393);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 32);
            this.label4.TabIndex = 11;
            this.label4.Text = "De la data:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Algerian", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label6.Location = new System.Drawing.Point(33, 323);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 45);
            this.label6.TabIndex = 13;
            this.label6.Text = "VANZARI:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Agency FB", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(35, 471);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 32);
            this.label7.TabIndex = 14;
            this.label7.Text = "Pana la data:";
            // 
            // cbMedicamente
            // 
            this.cbMedicamente.FormattingEnabled = true;
            this.cbMedicamente.Location = new System.Drawing.Point(30, 621);
            this.cbMedicamente.Name = "cbMedicamente";
            this.cbMedicamente.Size = new System.Drawing.Size(200, 24);
            this.cbMedicamente.TabIndex = 15;
            this.cbMedicamente.SelectedIndexChanged += new System.EventHandler(this.cbMedicamente_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Agency FB", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(35, 586);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 32);
            this.label5.TabIndex = 16;
            this.label5.Text = "Dupa Medicament:";
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Bodoni MT", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(1180, 296);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(90, 39);
            this.btnPrint.TabIndex = 17;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // inZiua
            // 
            this.inZiua.Location = new System.Drawing.Point(30, 704);
            this.inZiua.Name = "inZiua";
            this.inZiua.Size = new System.Drawing.Size(200, 22);
            this.inZiua.TabIndex = 18;
            this.inZiua.ValueChanged += new System.EventHandler(this.inZiua_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Agency FB", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 669);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 32);
            this.label8.TabIndex = 19;
            this.label8.Text = "Dupa Zi:";
            // 
            // Rapoarte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 749);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.inZiua);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbMedicamente);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panaLa);
            this.Controls.Add(this.deLa);
            this.Controls.Add(this.chart4);
            this.Controls.Add(this.dgvProducatori);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chartPie2);
            this.Controls.Add(this.lbProducatori);
            this.Controls.Add(this.chartPie1);
            this.Controls.Add(this.label1);
            this.Name = "Rapoarte";
            this.Text = "Grafic";
            ((System.ComponentModel.ISupportInitialize)(this.chartPie1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPie2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducatori)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPie1;
        private System.Windows.Forms.Label lbProducatori;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPie2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvProducatori;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.DateTimePicker deLa;
        private System.Windows.Forms.DateTimePicker panaLa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbMedicamente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.DateTimePicker inZiua;
        private System.Windows.Forms.Label label8;
    }
}