﻿namespace LicentaFINAL.Forms
{
    partial class SituatieStoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ViewSituatieStoc = new System.Windows.Forms.DataGridView();
            this.lbSearch = new System.Windows.Forms.Label();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.cbProducator = new System.Windows.Forms.ComboBox();
            this.ViewSituatieVanzari = new System.Windows.Forms.DataGridView();
            this.lbVanzari = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSearch2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbProducator2 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ViewSituatieStoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewSituatieVanzari)).BeginInit();
            this.SuspendLayout();
            // 
            // ViewSituatieStoc
            // 
            this.ViewSituatieStoc.AllowUserToOrderColumns = true;
            this.ViewSituatieStoc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ViewSituatieStoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ViewSituatieStoc.Location = new System.Drawing.Point(36, 83);
            this.ViewSituatieStoc.Name = "ViewSituatieStoc";
            this.ViewSituatieStoc.RowHeadersWidth = 51;
            this.ViewSituatieStoc.RowTemplate.Height = 24;
            this.ViewSituatieStoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ViewSituatieStoc.Size = new System.Drawing.Size(1250, 301);
            this.ViewSituatieStoc.TabIndex = 0;
            // 
            // lbSearch
            // 
            this.lbSearch.AutoSize = true;
            this.lbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSearch.Location = new System.Drawing.Point(593, 39);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(86, 25);
            this.lbSearch.TabIndex = 1;
            this.lbSearch.Text = "Search: ";
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(705, 42);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(258, 22);
            this.tbSearch.TabIndex = 2;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // cbProducator
            // 
            this.cbProducator.FormattingEnabled = true;
            this.cbProducator.Location = new System.Drawing.Point(1021, 40);
            this.cbProducator.Name = "cbProducator";
            this.cbProducator.Size = new System.Drawing.Size(258, 24);
            this.cbProducator.TabIndex = 3;
            this.cbProducator.SelectedIndexChanged += new System.EventHandler(this.cbProducator_SelectedIndexChanged);
            // 
            // ViewSituatieVanzari
            // 
            this.ViewSituatieVanzari.AllowUserToOrderColumns = true;
            this.ViewSituatieVanzari.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ViewSituatieVanzari.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ViewSituatieVanzari.Location = new System.Drawing.Point(36, 442);
            this.ViewSituatieVanzari.Name = "ViewSituatieVanzari";
            this.ViewSituatieVanzari.RowHeadersWidth = 51;
            this.ViewSituatieVanzari.RowTemplate.Height = 24;
            this.ViewSituatieVanzari.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ViewSituatieVanzari.Size = new System.Drawing.Size(1250, 295);
            this.ViewSituatieVanzari.TabIndex = 4;
            // 
            // lbVanzari
            // 
            this.lbVanzari.AutoSize = true;
            this.lbVanzari.Font = new System.Drawing.Font("Stencil", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVanzari.Location = new System.Drawing.Point(48, 387);
            this.lbVanzari.Name = "lbVanzari";
            this.lbVanzari.Size = new System.Drawing.Size(215, 51);
            this.lbVanzari.TabIndex = 6;
            this.lbVanzari.Text = "Vanzari:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Stencil", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 51);
            this.label2.TabIndex = 7;
            this.label2.Text = "Stoc:";
            // 
            // tbSearch2
            // 
            this.tbSearch2.Location = new System.Drawing.Point(705, 409);
            this.tbSearch2.Name = "tbSearch2";
            this.tbSearch2.Size = new System.Drawing.Size(258, 22);
            this.tbSearch2.TabIndex = 9;
            this.tbSearch2.TextChanged += new System.EventHandler(this.tbSearch2_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(593, 406);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Search: ";
            // 
            // cbProducator2
            // 
            this.cbProducator2.FormattingEnabled = true;
            this.cbProducator2.Location = new System.Drawing.Point(1021, 406);
            this.cbProducator2.Name = "cbProducator2";
            this.cbProducator2.Size = new System.Drawing.Size(258, 24);
            this.cbProducator2.TabIndex = 10;
            this.cbProducator2.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // SituatieStoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 749);
            this.Controls.Add(this.cbProducator2);
            this.Controls.Add(this.tbSearch2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbVanzari);
            this.Controls.Add(this.ViewSituatieVanzari);
            this.Controls.Add(this.cbProducator);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.lbSearch);
            this.Controls.Add(this.ViewSituatieStoc);
            this.Name = "SituatieStoc";
            this.Text = "SituatieStoc";
            ((System.ComponentModel.ISupportInitialize)(this.ViewSituatieStoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewSituatieVanzari)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ViewSituatieStoc;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.ComboBox cbProducator;
        private System.Windows.Forms.DataGridView ViewSituatieVanzari;
        private System.Windows.Forms.Label lbVanzari;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSearch2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbProducator2;
    }
}